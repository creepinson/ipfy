# IP For You

This is a python script that configures iptables port forwarding rules based on a yaml file.

## Requirements

```bash
pip3 install -r requirements.txt
```

## Using Rules

```bash
# ./ipfy.py <rule-file>.yaml <internal-ip> <interface>
./ipfy.py ./examples/minecraft.yaml 192.168.0.5 eth0
```
